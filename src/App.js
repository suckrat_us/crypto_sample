import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import './assets/style.css'
import PairSelector from './components/PairSelector';
import MainComponent from "./components/content-block";
import Time from './components/time'

const stocks = ['Binance', 'Bitfinex', 'Okex', 'Hitbtc', 'Bittrex', 'Exmo', 'Coinbase'];
//,'Lbank','Bitstamp','Bittrex','Bitforex'
// const stocks_fake = ['Lbank','Bitstamp','Bittrex','Bitforex','Bithumb', "Zb" , 'Lbank', 'Bcex' , 'Coinsuper' , 'Upbit' , 'Poloniex' , 'Exmo' ,'Pro.coinbase' , 'Kraken'];
// function RenderFakePageComponent(pair) {
//     const stockItems = [];
//     for (let i = 0; i < stocks_fake.length; i++) {
//         stockItems.push(<MainComponentFake pair={pair} stocks={stocks_fake[i]} stocks_num={i+4}/>);
//     }
//     return stockItems;
// }


function RenderFirstPageComponent(pair) {
    const stockItems = [];
    for (let i = 0; i < stocks.length; i++) {
        stockItems.push(<MainComponent pair={pair} stocks={stocks[i]} stocks_num={i}/>);
    }
    return stockItems;
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            yourPick: localStorage.getItem("pair") ? localStorage.getItem("pair") : "btc/usd",
            add_thumb: JSON.parse(localStorage.getItem("add_thumb"))
        };
        this.stateHandler = this.stateHandler.bind(this);
    }

    stateHandler(props) {
        this.setState({
            yourPick: props
        });
        ReactDOM.unmountComponentAtNode(document.getElementById('MainComponent'));
        ReactDOM.render(RenderFirstPageComponent(props), document.getElementById("MainComponent"));
    }
    render() {
      this.interval = setInterval(
            () => {
                window.location.reload();
            }, 180000);
        return (
            <div className="web-body">
                <div className="header">
                    <div className="top-part">
                        <div className="time">
                            <Time/>
                        </div>
                    </div>
                    <div className="bot-part">
                        <div className="button-container">
                            <PairSelector handle={this.stateHandler}/>
                        </div>
                    </div>
                </div>

             <div id='MainComponent' className="content-window">
                        {RenderFirstPageComponent(this.state.yourPick)}
              </div>


            </div>
        )
    }
}

export default App;
