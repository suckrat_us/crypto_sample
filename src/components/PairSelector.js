import React, {Component} from 'react';
import '../assets/PairSelector.css';

class PairSelector extends Component{
    constructor(props) {
        super(props)
        this.state = {
            aviable_pairs: ["btc/usd", 'eth/usd', "ltc/btc", "etc/btc", "bch/btc", "eos/btc", "ada/btc", "xrp/btc",
                "zec/btc","dash/btc","xlm/btc","eth/btc",'trx/btc','xem/btc','lsk/btc','qtum/btc', 'iota/btc', 'xmr/btc'],
            yourPick: localStorage.getItem("pair")
        }
    }
    render() {
        const options = this.state.aviable_pairs.map((loan, key) => {
            const isCurrent = this.state.yourPick === loan;
            return (
                <div key={key} className="radioPad">
                    <div>
                        <label className={
                            isCurrent ?
                                'radioPad__wrapper radioPad__wrapper--selected' :
                                'radioPad__wrapper'
                        }>
                            <input className="radioPad__radio" type="radio" name="aviable_pairs" id={loan} value={loan} onChange={this.handleRadio.bind(this)}/>
                            {loan}
                        </label>
                    </div>
                </div>
            )
        });
        return (
            <div className="container text-center">
                <div className="row">
                    {options}
                </div>
            </div>
        )
    }
    handleRadio(e) {
        this.setState({yourPick: e.target.value });
        localStorage.setItem("pair",e.target.value);
        this.props.handle(e.target.value);
        window.location.reload();
    }
}
export default PairSelector