import React from 'react'
import Clock from 'react-live-clock';
import $ from 'jquery'
import ReactTable from "react-table";
import axios from 'axios'
import moment from 'moment'

import '../assets/comp_style.css';
import '../assets/spinner.css';
import 'react-table/react-table.css';


const TIME_INTERVALS = ['TIME', '1 d', '10 s', '30 s', '1 m', '5 m', '15 m', '30 m', '1 h', '4 h', '2 d', '7 d'];
const TIME_PERIODS = ['TIME', 10000, 10000, 30000, 60000, 300000, 600000, 1200000, 4800000, 14400000, 172800000, 604800000];

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

let cancel;
let query;
class Column_time_comp extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            date: 1,
            sell: 0,
            buy: 0,
            sell_v: 0,
            buy_v: 0,
            price: 0,

            old_sell: 'loading...',
            old_buy: 'loading...',
            old_sell_v: 'loading...',
            old_buy_v: 'loading...',
            old_price: 'loading...',

            hours24: 0,
            hours48: 0,
            hours72: 0,
            hours96: 0,
            time: this.props.time / 1000,
            pair: this.props.pair,
            volume: 0,
            color_vol: ''
        };
        if (this.props.col === '1 d') {
            query = 'http://some_api.ru/api/' + this.props.stocks.toLowerCase() + '/interval?pair=' + this.props.pair + '&start='
                + moment.utc().startOf('day').unix() + '&end=' + moment.utc().unix();
        } else {
            query = 'http://some_api.ru/api/' + this.props.stocks.toLowerCase() + '?sec=' + this.state.time + '&pair=' + this.props.pair;
        } 
        if (!isNaN(this.state.time)) {
            axios.get((query), {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                cancelToken: new CancelToken(function executor(c) {
                    // An executor function receives a cancel function as a parameter
                    cancel = c;
                })

                })
                .then(response => {
                    this.setState({
                        sell: response.data.countSell,
                        buy: response.data.countBuy,
                        sell_v: response.data.sumSell,
                        buy_v: response.data.sumBuy,
                        price: response.data.price,

                        old_sell: response.data.oldCountSell,
                        old_buy: response.data.oldCountBuy,
                        old_sell_v: response.data.oldSumSell,
                        old_buy_v: response.data.oldSumBuy,
                        old_price: response.data.oldPrice,

                        hours24: response.data.price24H,
                        color: response.data.countSell > response.data.countBuy,
                        volume: response.data.sumSell+response.data.sumBuy,

                        color : response.data.countSell > response.data.countBuy,
                        color_vol: ''
                    });
                })
                .catch(error => console.log(error));
        }
    }

    componentDidMount() {
        this.state = {
            sell: 0,
            buy: 0,
            price: 0,
            hours24: 0
        };

    }

    componentWillUnmount() {
        clearInterval(this.interval);
        cancel();


    }

    componentDidUpdate() {
        clearInterval(this.interval);

    }

    render() {
        let str;
        let time; 
        console.log(this.state.time);
        if (this.props.col === '1 d') {
            str = 'http://some_api.ru/api/' + this.props.stocks.toLowerCase() + '/interval?pair=' + this.props.pair + '&start='
                + moment.utc().startOf('day').unix() + '&end=';
            time = 60000;
        } else {
            str = 'http://some_api.ru/api/' + this.props.stocks.toLowerCase() + '?sec=' + this.state.time + '&pair=' + this.props.pair;
            time = this.state.time * 1000;
        } 
        this.interval = setInterval(
            () => {
                if (!isNaN(this.state.time)) {
                    if(this.props.col === '1 d') {
                        query = str + moment().unix();
                    } else {
                        query = str;
                    }
                    axios.get((query), {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        cancelToken: source.token

                    })
                        .then(response => {
                            this.setState({
                                sell: response.data.countSell,
                                buy: response.data.countBuy,
                                sell_v: response.data.sumSell,
                                buy_v: response.data.sumBuy,
                                price: response.data.price,

                                old_sell: response.data.oldCountSell,
                                old_buy: response.data.oldCountBuy,
                                old_sell_v: response.data.oldSumSell,
                                old_buy_v: response.data.oldSumBuy,
                                old_price: response.data.oldPrice,

                                hours24: response.data.price24H,
                                color: response.data.countSell > response.data.countBuy,
                                volume: response.data.sumSell+response.data.sumBuy,

                                color_vol: response.data.sumSell + response.data.sumBuy > this.state.volume ? '-green' : '-red'
                            });
                        })
                        .catch(error => console.log(error));
                }
            }, time);

        if(this.state.sell>this.state.buy) {
           document.getElementById(this.props.col + this.props.key_v.toString()).parentNode.parentNode.style.background = "rgba(255, 0, 16, 0.26)";
        }
        if(this.state.sell<this.state.buy) {
            document.getElementById(this.props.col + this.props.key_v.toString()).parentNode.parentNode.style.background = "rgba(0, 255, 25, 0.26)";
        }

        if (this.props.col == 'TIME') {
            return (
                <div>
                    <div className='cell'>Price</div>
                    <div className='cell'>Sell</div>
                    <div className='cell'>Sell_vol</div>
                    <hr />
                    <div className='cell'>Buy</div>
                    <div className='cell'>Buy_vol</div>
                    <div className='cell'>Volume</div>
                    <div className='cell'>24H</div>
                    
                </div>
            )
        }
        else if(this.props.col === '1 d'){

            return (
                    <div>
                        <div className={'inline'}>
                            <div className='cell-cent'>{parseFloat(this.state.price).toFixed(6)}</div>
                        </div>

                        <div className={'inline'}>
                            <div className={'cell-cent'}>{this.state.sell}%</div>
                        </div>

                        <div className={'inline'}>
                            <div className={'cell-cent'+(this.state.color? '-red':'')}>{parseFloat(this.state.sell_v).toFixed(1)}</div>
                        </div>
                        <hr />

                        <div className={'inline'}>
                            <div className={'cell-cent'}>{this.state.buy}%</div>
                        </div>


                        <div className={'inline'}>
                            <div className={'cell-cent'+(this.state.color? '' :'-green')}>{parseFloat(this.state.buy_v).toFixed(1)}</div>
                        </div>

                        <div className={'cell-cent'+(this.state.color_vol)}>{parseFloat(this.state.volume).toFixed(3)}</div>
                        <div className='cell-cent'>{this.state.hours24}</div>
                    </div>
                )

        }
        else {
            const options = {
                followCursor: true,
                shiftX: 0,
                shiftY: -80
            }
            
            return (
                <div>
                    <div className={'inline'}>
                        <div className='cell-cent prev'>{parseFloat(this.state.old_price).toFixed(6)}</div>
                        <div className='cell-cent'>{parseFloat(this.state.price).toFixed(6)}</div>
                    </div>

                    <div className={'inline'}>
                        <div className={'prev cell-cent'}>{(this.state.old_sell)}</div>
                        <div className={'cell-cent'}>{this.state.sell}%</div>
                    </div>

                    <div className={'inline'}>
                        <div className={'prev cell-cent'}>{parseFloat(this.state.old_sell_v).toFixed(1)}</div>
                        <div className={'cell-cent'+(this.state.color? '-red':'')}>{parseFloat(this.state.sell_v).toFixed(1)}</div>
                    </div>
                    <hr />

                    <div className={'inline'}>
                        <div className={'prev cell-cent'}>{(this.state.old_buy)}</div>
                        <div className={'cell-cent'}>{this.state.buy}%</div>
                    </div>


                    <div className={'inline'}>
                        <div className={'prev cell-cent'}>{parseFloat(this.state.old_buy_v).toFixed(1)}</div>
                        <div className={'cell-cent'+(this.state.color? '':'-green')}>{parseFloat(this.state.buy_v).toFixed(1)}</div>
                    </div>

                    <div className={'cell-cent'+(this.state.color_vol)}>{parseFloat(this.state.volume).toFixed(3)}</div>
                    <div className='cell-cent'>{this.state.hours24}</div>
                </div>
            )
        }
    }
}

function m_array_of_col_comp(time_name, time, pair, stock, stocks_num) {
    var rv = {};
    for (var i = 0; i < time_name.length; ++i)
        rv[time_name[i]] = <Column_time_comp pair={pair} stocks={stock} col={time_name[i]} time={time[i]} key_v={stocks_num}/>;
    return rv;
}

export default class MainComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pair: props.pair,
            stocks_num:props.stocks_num
        };

    }
    componentWillReceiveProps(nextProps) {
        this.setState({pair:nextProps})
    }

    render() {

        const data = [m_array_of_col_comp(TIME_INTERVALS, TIME_PERIODS, this.props.pair, this.props.stocks,this.state.stocks_num)];
        const columns = TIME_INTERVALS.map(
            (name) => {
                return (
                    {
                        Header: () => (
                            <div id={name+this.state.stocks_num}>{name}</div>
                        ),
                        id: name,
                        accessor: name,
                    }
                )
            }
        );
        return (
            <div className='stocks-block'>
                <div className={"clock_and_name"}>
                    <div className='stocks-name-row'>
                        <div className="time-head">{<Clock format={'HH:mm:ss'} ticking={true}/>}</div>
                        <Spinner/>
                    </div>
                    <div className={"CenterName"}>{this.props.stocks} {this.props.pair.toUpperCase()} 
                    {
                        this.props.stocks==="Binance"? <div>   +GTM 3</div>
                            : this.props.stocks==="Bitfinex"?<div>   GTM 0</div>
                            : this.props.stocks==="Okex"?<div>  +GTM 3</div>
                            : <div>  +GTM 3</div>

                    }
                    </div>
                    <div className={'RightName'}>
                    {
                        this.props.stocks==="Binance"? <div><a href='https://www.binance.com/'>www.binance.com</a></div>
                            : this.props.stocks==="Bitfinex"?<div><a href='https://www.bitfinex.com/'>www.bitfinex.com</a></div>
                            : this.props.stocks==="Okex"?<div><a href='https://www.okex.com/'>www.okex.com</a></div>
                            : this.props.stocks==="Hitbtc"?<div><a href='https://hitbtc.com/'>www.hitbtc.com</a></div>
                            : this.props.stocks==="Bittrex"?<div><a href='https://bittrex.com/'>www.bittrex.com</a></div>
                            : this.props.stocks==="Exmo"?<div><a href='https://exmo.com/'>www.exmo.com</a></div>
                            : this.props.stocks==="Coinbase"?<div><a href='https://www.coinbase.com/'>www.coinbase.com</a></div>
                            : <div></div>

                    }
                    </div>
                </div>
                <ReactTable
                    data={data}
                    columns={columns}
                    defaultPageSize={1}
                    showPaginationBottom={false}
                    sortable={false}
                />
            </div>
        )
    }
}

function spinCount(selector) {
    let $selector = $(selector);
    let i = 0;
    setInterval(function () {
        i++;
        if (i < 10){
            $selector.find('.spinner>span').css('animation','none');
        }
        else {
            $selector.find('.spinner>span').css('backgound-color','orange');

        }
    },1000);
}


class Spinner extends React.Component{
    render() {
        {spinCount()}
        return(
            <span className="spinner" id='spin'><span></span><span></span><span></span></span>
        )
    }
}