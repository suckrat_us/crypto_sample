import React from 'react'
import Clock from 'react-live-clock';



export default class Time extends React.Component {
    render() {
        return(
        <Clock format={'HH:mm:ss  DD.MM.YYYY'} ticking={true} />
        )
    }
}